# Info

* Minden "határidő"-t értsünk úgy, hogy a megadott nap 23:59 időpontjáig adható le a kész munka
* A megoldást a Canvas felületen adjátok le (méltányolható okokból lehet kivétel, ekkor emailben kérem)
* A határidőn túl beadott megoldásokat is átnézem, Canvasban ezeket is értékelem szövegesen - tanulni lehet belőle -, de arra pontot már nem adok
* Hacsak más nincs mondva, minden feladat 1 pontot ér (de ez Canvason is látszik)

# 02_BevezetoPeldak témakörhöz

## 01_BubbleSortCserek

* Határozzuk meg a buborékrendezés tanult verziójára az mswap<sub>bubbleSort</sub>, az Mswap<sub>bubbleSort</sub> és az Aswap<sub>bubbleSort</sub> értékeket - mindhárom helyes: 1 pont
* Bizonyítsuk is be a fentieket (lehet "esszé-jellegű", de legyen pontos, alapos) - mindhárom helyes: 1 pont
* Határidő: 2020. 04. 18.

## 02_BubbleSortJavitas

* Írd meg a buborékrendezés hatékonyabb verzióját az alábbi elgondolást követve:
    * 0-tól indexelt tömböt használj, az órán tanult algoritmusból indulj ki
    * Ha az adott külső körben az utolsó néhány vizsgált elem körében már nem volt csere, akkor azok az elemek egymáshoz képest sorrendben vannak. De az is biztos, hogy ezek az elemek nagyobbak minden őket megelőző elemnél, hiszen ha nem így lett volna, egy ilyen nagyobb elem felbuborékoltatott volna az utolsó elemek közé (netán utánuk)
    * Az órán tanult változat invariánsa csak annyit mondott: mindig egy elemmel több a már biztosan rendezett résztömb. Ez a verzió ennél többet tud, következésképpen hatékonyabb... mennyivel is?
* 1 pont jár a helyes struktogramért
* Még egy pont jár, ha megadod az m, M, A értékeket a compare (összehasonlítás) domináns műveletre, rövid szöveges érveléssel
* (A progalapos diasor nyújthat kis segítséget)
* Határidő: 2020. 04. 18.

## 03_LegendreSzorzas

* Határozzuk meg a tanult Legendre-algoritmus alapján az mmult<sub>legendre</sub>(a, k), Mmult<sub>legendre</sub>(a, k) és Amult<sub>legendre</sub>(a, k) értékeket (azaz a szorzások számát)
* Indokoljunk is meg a fentieket konkrét a-kkal, k-kkal, levezetve, hogy jött ki a válasz
* Az algoritmusban látható osztás nem számít szorzásnak, csak a "csillag"-gal jelölt műveletet számoljuk
* Határidő: 2020. 04. 18.

# 03_Muveletigeny témakörhöz

## 04_ThetaEkvRel

* Bizonyítsuk be, hogy a Θ-viszony ekvivalenciareláció, azaz:
    * reflexív
    * tranzitív
    * szimmetrikus
* Határidő: 2020. 04. 18.

## 05_OrdoOmegaEkvRel

* Vajon ekvivalenciareláció-e Ο és Ω? Döntsük el, és bizonyítsuk!
* Határidő: 2020. 04. 18.
