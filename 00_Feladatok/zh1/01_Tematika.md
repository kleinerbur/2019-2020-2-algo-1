# Első zh

## Tematika

* Algoritmusok, adatszerkezetek (01_Bevezetes)
    * Fogalmak ismerete, egyszerűbb összefüggések, "elméleti kérdések"
    * Primitív és összetett típusok - megkülönböztetés, használat
    * Tömbök tulajdonságai, megkötései, használatuk, indextartományok
    * Pointerek - heap vs. stack
    * Szignatúra, formális és aktuális paraméterlista
    * Algoritmus típusok: metódus, tiszta függvény, mellékhatás, paraméterátadási módok
    * Strukturált programozás
    * Invariáns, típusinvariáns
    * Rekurzió, rekurzió segédfüggvénnyel
    * Tárigény
* Bevezető példák (02_BevezetoPeldak)
    * Buborékrendezés - feladat, módszer, lejátszás, algoritmus, javítás
    * Hanoi tornyai - feladat, módszer, lejátszás, algoritmus
    * Polinomszámolás - feladat, a 3 féle stratégia és algoritmus
    * Legendre-algoritmus - feladat, algoritmus
* Műveletigény (03_Muveletigeny)
    * Jelölések (m, M, A), jelentésük
    * Algoritmus konkrét műveletigényének levezetése (pl. a tanult bevezető példáknál)
    * Θ, O, o, Ω, ω halmazok
    * Műveletigényhalmazba tartozás definícióból, azonosságokkal
    * Nagyságrendosztályokba sorolás, nagyságrend szerinti sorrend

## Formátum

* Online
* Algoritmusíros feladat - a példák nyomán, esetleg tömbös egyszerűbb algoritmus
    * Műveletigénye
    * Szintaktikai helyességre, paraméterezésre figyelni
    * Keresztkérdések
* Műveletigényekkel kapcsolatos feladat
    * Osztályba sorolás (akár a tanult függvényeket is)
    * Levezetés, bizonyítás megadott eszközökkel
    * Keresztkérdések
* x perc, 20 pont

## A törzsanyagon túli segédanyagok

* Konzultacio.md
* Korábbi félévek zh-inak vonatkozó feladatai
    * Vigyázat, a jelölések nagyban eltérhetnek az idei elvártaktól

# Ide tartozó szorgalmik